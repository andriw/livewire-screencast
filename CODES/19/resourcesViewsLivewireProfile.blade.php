- <div class="flex justify-end">

+            <div class="space-x-3 flex justify-end items-center">
                <span
                    x-data="{ open: false }"
                    x-init="
                        @this.on('notify-saved', () => {
                            if (open === false) setTimeout(() => { open = false }, 2500);
                            open = true;
                        })
                    "
                    x-show.transition.out.duration.1000ms="open"
                    style="display: none;"
                    class="text-gray-500"
               >Saved!</span>
+

- <span class="ml-3 inline-flex rounded-md shadow-sm">
+ <span class="inline-flex rounded-md shadow-sm">